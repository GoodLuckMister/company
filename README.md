### Quick Setup

#### 1. Create a Virtual Machine (VM)
- **Cloud Provider**: While any cloud provider can be used, we recommend Digital Ocean.
- **Specifications**: A VM with 2 CPUs and 4 GB of RAM should suffice.
- **Docker**: Ensure Docker (version >= 22) is installed on your VM.
- **Ports**: Ports 80 and 443 should be open on your VM.

#### 2. Docker login
```bash
docker login registry.activechat.ai -u <username> -p <your-password> 
```

#### 3. Download and untar distro
```bash
tar xzvf onpremcom.tar.gz
cd onpremcom
```

#### 4. Edit `config.env` and update your DNS records.
Please, 
```bash
cp config.env.orig config.env
``` 
and then edit it accroding your domain and mongo settings.  
To ensure functionality, we require 7 domain names that point to your VM's IP address:
- CONSTRUCTOR_DOMAIN
- ACCESS_DOMAIN
- LIVECHAT_DOMAIN
- CHATWIDGET_DOMAIN
- ARCHITECTOR_DOMAIN
- CIS_DOMAIN
- USER_DOMAIN

#### 5. Start Your Application
Execute the following command to start your application:
```bash
./start.sh
```
Please, note, first start will take up to 15 minutes.
