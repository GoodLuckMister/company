#!/bin/bash
set -e
mkdir -p data/db
set -o allexport
source config.env
set +o allexport
docker compose up -d
